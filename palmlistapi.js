const express = require('express');
const app = express();

const ports = [2010, 2011];
const bodyParser = require('body-parser');

const jwt = require('jsonwebtoken');

const auth = require('./auth/auth');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

const provider = require('./routes/provider');
const service = require('./routes/service');
const request = require('./routes/request');
const client = require('./routes/client');
const user = require('./routes/user');

app.use((req, res, next) => {
    let _token = req.headers['x-api-key'];
    if (_token) {
        jwt
            .verify(_token, 'secret', function (err, decode) {
                if (err)
                    req.token = undefined;
                req.token = decode;
                next();
            });
    } else {
        req.token = undefined;
        next();
    }
});

app.use('/', user.router);
app.use('/', provider);
app.use('/', client);
app.use('/', service);
app.use('/', request);

ports.forEach((port, i) => {
    app.listen(port, () => {
        console.log('api listening on port %s', port);
    });
});