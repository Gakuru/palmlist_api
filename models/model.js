module.exports = {
    provider: {
        firstName: {
            required: true,
            knownName: 'firstName'
        },
        lastName: {
            required: true,
            knownName: 'lastName'
        },
        email: {
            required: true,
            knownName: 'email'
        },
        phone: {
            required: true,
            knownName: 'phone'
        },
        password: {
            required: true,
            knownName: 'password'
        },
        geo_lat: {
            required: false,
            knownName: 'lat'
        },
        geo_long: {
            required: false,
            knownName: 'lng'
        },
        geo_accuracy: {
            required: false,
            knownName: 'geo_accuracy'
        }
    },
    service: {
        name: {
            required: true,
            knownName: 'name'
        },
        created_by: {
            required: false,
            knownName: 'provider_id'
        },
        tags: {
            required: false,
            knownName: 'tags'
        }
    },
    provider_services: {
        service_id: {
            required: true,
            knownName: 'sid'
        },
        provider_id: {
            required: true,
            knownName: 'pid'
        }
    },
    client: {
        first_name: {
            required: true,
            knownName: 'firstName'
        },
        last_name: {
            required: true,
            knownName: 'lastName'
        },
        phone: {
            required: true,
            knownName: 'phone'
        },
        email: {
            required: true,
            knownName: 'email'
        },
        password: {
            required: true,
            knownName: 'password'
        }
    },
    providersNearBy: {
        provider_name: {
            required: true,
            knownName: 'name'
        },
        lat: {
            required: true,
            knownName: 'lat'
        },
        lng: {
            required: true,
            knownName: 'lng'
        },
        distance: {
            required: true,
            knownName: 'distance'
        }
    }
}