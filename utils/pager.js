module.exports = {
    options: {
        offSet: 0,
        size: 2
    },
    getPager: (resultLength, totalResultLength, pageSize, nextStart, url) => {

        let nextStartAt = parseInt(nextStart) + 1;
        let currStartAt = (nextStartAt - 1);
        let hasNext = (totalResultLength > resultLength
            ? true
            : false);
        let hasPrev = ((resultLength === 0)
            ? false
            : ((nextStartAt - 1) == 1)
                ? false
                : true);
        return {
            hasNext: hasNext,
            hasPrev: hasPrev,
            currStartAt: currStartAt,
            nextStartAt: nextStartAt,
            nextPageUrl: (hasNext
                ? url + '/page/' + nextStartAt
                : null),
            currentPageUrl: (!(hasNext && hasPrev))
                ? (nextStart > 1
                    ? `${url}/page/${ (nextStartAt - 1)}`
                    : url)
                : `${url}/page/${ (nextStartAt - 1)}`,
            prevPageUrl: (hasPrev
                ? url + '/page/' + (nextStartAt - 2)
                : null),
            pageSize: pageSize
        }
    }
};