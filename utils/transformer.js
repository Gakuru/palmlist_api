const urls = require('./urls');

const model = require('../models/model')

module.exports = {
    transformProvider: (providers) => {
        if (providers) {
            const {firstName, lastName, email, phone} = model.provider;
            return providers.map((provider) => {
                return {
                    id: provider
                        .id
                        .toString(),
                    [firstName.knownName]: provider
                        .firstName
                        .toString(),
                    [lastName.knownName]: provider
                        .lastName
                        .toString(),
                    [email.knownName]: provider
                        .email
                        .toString(),
                    [phone.knownName]: provider
                        .phone
                        .toString(),
                    link: `${urls.providerUrl}/${provider.id}`
                }
            })
        }

        return providers;
    },
    transformService: (services) => {
        if (services) {
            const {name, tags} = model.service;
            return services.map((service) => {
                return {
                    id: service
                        .id
                        .toString(),
                    [name.knownName]: service
                        .name
                        .toString(),
                    [tags.knownName]: service.tags
                        ? service
                            .tags
                            .toString()
                        : null,
                    link: `${urls.serviceUrl}/${service.id}`
                }
            })
        }

        return services;
    },
    transformClient: (clients) => {
        if (clients) {
            const {first_name, last_name, email, phone} = model.client;
            return clients.map((client) => {
                return {
                    id: client
                        .id
                        .toString(),
                    [first_name.knownName]: client
                        .first_name
                        .toString(),
                    [last_name.knownName]: client
                        .last_name
                        .toString(),
                    [email.knownName]: client
                        .email
                        .toString(),
                    [phone.knownName]: client
                        .phone
                        .toString(),
                    link: `${urls.clientUrl}/${client.id}`
                }
            })
        }

        return clients;
    },
    transformProvidersNearBy: (providers) => {
        if (providers) {
            const {provider_name, lat, lng, distance} = model.providersNearBy;
            return providers.map((provider) => {
                return {
                    id: provider
                        .provider_id
                        .toString(),
                    [provider_name.knownName]: provider
                        .provider_name
                        .toString(),
                    [lat.knownName]: provider
                        .lat
                        .toString(),
                    [lng.knownName]: provider
                        .lng
                        .toString(),
                    [distance.knownName]: provider.distance
                }
            })
        }

        return providers;
    }
}