const urls = require('../../.constants/url');
const baseUrl = urls.baseUrl;

module.exports = {
    providerUrl: `${baseUrl}providers`,
    serviceUrl: `${baseUrl}services`,
    clientUrl: `${baseUrl}clients`
}