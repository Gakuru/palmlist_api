const express = require('express');
const router = express.Router();

const provider = require('../modules/provider');

const auth = require('../auth/auth');

router.get('/', (req, res) => {
  res.send('Palmlist');
});

router.get('/providers', (req, res) => {
  provider.get({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/providers/nearby', (req, res) => {
  provider.getNearBy({
    coordinates: req.query,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/providers/page/:page', (req, res) => {
  provider.get({
    page: parseInt(req.params.page),
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/providers/:id/services', (req, res) => {
  provider.getProviderServices({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/providers/:id', (req, res) => {
  provider.get({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/providers/requests/:id', (req, res) => {
  provider.getProviderRequests({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/provider', (req, res) => {
  provider.save({
    data: {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      phone: req.body.phone,
      password: req.body.password,
      geo_lat: req.body.geo_lat,
      geo_long: req.body.geo_long,
      geo_accuracy: req.body.geo_accuracy
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/provider/addservice', (req, res) => {
  provider.addService({
    data: {
      service_id: req.body.sid,
      provider_id: req.body.pid
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/provider/removeservice', (req, res) => {
  provider.removeService({
    data: {
      id: req.body.id,
      provider_id: req.body.pid
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

module.exports = router;