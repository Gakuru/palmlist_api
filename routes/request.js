const express = require('express');
const router = express.Router();

const request = require('../modules/request');

router.get('/', (req, res) => {
  res.send('Palmlist');
});

router.get('/request', (req, res) => {
  request.getServiceProvidersNearby({
    data: req.query,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/request/requestservice', (req, res) => {
  request.request({
    data: {      
      client_lat: req.body.lat,
      client_lng: req.body.lng,
      service_id: req.body.sid,
      provider_id: req.body.pid,
      client_id: req.body.uid
    },
    callback: (result) => {
      res.json(result);
    }
  })
});

module.exports = router;