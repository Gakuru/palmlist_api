const express = require('express');
const router = express.Router();

const client = require('../modules/client');

const auth = require('../auth/auth');

router.get('/', (req, res) => {
  res.send('Palmlist');
});

router.get('/clients', (req, res) => {
  client.get({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/clients/page/:page', (req, res) => {
  client.get({
    page: parseInt(req.params.page),
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/clients/:id', (req, res) => {
  client.get({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/client', (req, res) => {
  client.save({
    data: {
      first_name: req.body.firstName,
      last_name: req.body.lastName,
      phone: req.body.phone,
      email: req.body.email,
      password: req.body.password
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/provider/addservice', (req, res) => {
  client.addService({
    data: {
      service_id: req.body.sid,
      provider_id: req.body.pid
    },
    callback: (result) => {
      res.json(result);
    }
  })
});

module.exports = router;