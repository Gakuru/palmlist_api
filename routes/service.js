const express = require('express');
const router = express.Router();

const service = require('../modules/service');

router.get('/', (req, res) => {
    res.send('Palmlist');
});

router.get('/services', (req, res) => {
    service.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/services/search', (req, res) => {
    service.searchServices({
        data: req.query,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/services/search/toadd', (req, res) => {
    service.searchServices({
        data: req.query,
        toAdd:true,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/services/page/:page', (req, res) => {
    service.get({
        page: parseInt(req.params.page),
        callback: (result) => {
            res.json(result);
        }
    });
});

router.get('/services/:id', (req, res) => {
    service.get({
        id: req.params.id,
        callback: (result) => {
            res.json(result);
        }
    });
});

router.post('/service', (req, res) => {
    service.save({
        data: {
            name: req.body.name,
            tags: req.body.tags
        },
        callback: (result) => {
            res.json(result);
        }
    });
});

module.exports = router;