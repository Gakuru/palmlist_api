const connection = require('../db/connect');

module.exports = {
    getUser: (user, callback) => {
        connection.query('CALL `get_user` (?,?)', [
            user.email, user.password
        ], (err, result, fields) => {
            if (err) 
                throw err;
            
            let _user = result[0][0];

            if (_user) {
                callback({
                    id: _user.id,
                    lastName: _user.lastName,
                    email: _user.email,
                    type: _user.dicriminator
                }, true);
            } else {
                callback({}, false);
            }
        });
    }
};