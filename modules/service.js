const connection = require('../db/connect');

const urls = require('../utils/urls');

const uuid = require('uuid/v1');

const stringUtils = require('../utils/stringUtils');

const pager = require('../utils/pager');
const transformer = require('../utils/transformer');

const validate = require('../utils/dataValidator');

const errorHandler = require('../utils/error');

var messages = [];

const returnPaged = (result, nextStart, totalResultLength) => {
    return {
        pager: pager.getPager(result.length, totalResultLength, pager.options.size, nextStart, urls.serviceUrl),
        messages: messages,
        data: transformer.transformService(result)
    };
};

module.exports = {
    get: (args) => {

        messages = [];

        if (args.id) {
            connection.query("CALL `get_service` (?);", [args.id], (err, results, fields) => {
                if (err) 
                    throw err;
                
                args.callback(transformer.transformService(results[0]));

            })
        } else {

            if (args.page < 0) {
                messages.push(`Negative page value reset [${args.page} reset to ${ 1}]`)
            }

            args.page = args.page
                ? (args.page < 0)
                    ? 1
                    : args.page
                : 1;

            pager.options.offSet = ((args.page * pager.options.size) - pager.options.size);

            connection.query("CALL `get_services` (?,?);", [
                pager.options.offSet,
                (pager.options.size + 1)
            ], (err, results, fields) => {
                if (err) 
                    throw err;
                
                let totalResultLength = results[0].length;
                let resultSet = ((totalResultLength > pager.options.size)
                    ? results[0].splice(0, pager.options.size)
                    : results[0]);
                args.callback(returnPaged(resultSet, args.page, totalResultLength))

            })
        }

    },
    searchServices: (args) => {
        const {data,toAdd, callback} = args;
        let response = undefined;

        const query = toAdd ? "CALL `find_provider_services_to_add` (?,?);" : "CALL `find_service` (?);";
        const queryParams = toAdd ? [data.service,data.pid] : [data.service];

        connection.query(query, queryParams , (err, results, fields) => {

            if (err) 
                throw err

            response = {
                action: 'List services',
                status: 'ok',
                code: 200,
                message: 'Listing services',
                results: {
                    services: results[0]
                }
            }

            args.callback(response);

        });
    },
    save: (args) => {

        const {data, callback} = args;

        let response = undefined;
        if (data.id) {

            callback('update');

        } else {

            let dataValidator = validate.validatePost(data, 'service');

            if (dataValidator.isValid) {

                data.id = uuid();

                if (data.tags) 
                    data.tags = stringUtils.replaceSpaceWithComma(data.tags);
                
                connection.getConnection((err, connection) => {

                    connection.beginTransaction((err) => {

                        connection.query('INSERT INTO `service` SET ?', data, (err, results, fields) => {
                            if (err) {
                                response = {
                                    action: 'Create service',
                                    status: ' failed ',
                                    code: 400,
                                    err: errorHandler.throwSQLErr(err)
                                }
                            } else {
                                data.link = `${urls.serviceUrl}/${data.id}`;
                                response = {
                                    action: 'Create service',
                                    status: 'ok',
                                    code: 200,
                                    message: 'service created',
                                    results: {
                                        provider: data
                                    }
                                }
                            }

                        });

                        // Commit
                        connection.commit((err) => {
                            if (err) {
                                return connection.rollback(() => {
                                    response = {
                                        action: 'Create service',
                                        status: 'failed',
                                        code: 400,
                                        err: errorHandler.throwSQLErr(err)
                                    }
                                });
                            } else {
                                callback(response);
                            }
                        });

                    });

                    connection.release();

                });

            } else {
                response = {
                    action: 'create service',
                    status: 'failed',
                    code: 400,
                    err: {
                        message: dataValidator.message,
                        resoulution: dataValidator.resolution,
                        err: dataValidator.err
                    }
                }

                callback(response);
            }
        }
    }
}