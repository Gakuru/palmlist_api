const connection = require('../db/connect');

const uuid = require('uuid/v1');

const urls = require('../utils/urls');

const pager = require('../utils/pager');
const transformer = require('../utils/transformer');

const validate = require('../utils/dataValidator');

const errorHandler = require('../utils/error');

const returnPaged = (result, nextStart, totalResultLength) => {
    return {
        pager: pager.getPager(result.length, totalResultLength, pager.options.size, nextStart, urls.providerUrl),
        data: transformer.transformClient(result)
    };
};

module.exports = {
    get: (args) => {
        if (args.id) {
            connection.query('CALL `get_client` (?);', [args.id], (err, results, fields) => {
                if (err) 
                    throw err;
                
                args.callback(transformer.transformClient(results[0]));

            })
        } else {

            args.page = args.page
                ? (args.page < 0)
                    ? 1
                    : args.page
                : 1;

            pager.options.offSet = ((args.page * pager.options.size) - pager.options.size);

            connection.query('CALL `get_clients` (?,?);', [
                pager.options.offSet,
                (pager.options.size + 1)
            ], (err, results, fields) => {
                if (err) 
                    throw err;
                
                let totalResultLength = results[0].length;
                let resultSet = ((totalResultLength > pager.options.size)
                    ? results[0].splice(0, pager.options.size)
                    : results[0]);

                args.callback(returnPaged(resultSet, args.page, totalResultLength))

            })
        }
    },
    save: (args) => {

        const {data, callback} = args;

        let response = undefined;

        if (data.id) {
            callback('update');
        } else {

            let dataValidator = validate.validatePost(data, 'client');

            if (dataValidator.isValid) {

                data.id = uuid();

                connection.query('INSERT INTO `client` SET ?;', {
                    id: data.id,
                    first_name: data.first_name,
                    last_name: data.last_name,
                    phone: data.phone,
                    email: data.email
                }, (err, results, fields) => {
                    if (err) {

                        response = {
                            action: 'Create client',
                            status: 'failed',
                            code: 400,
                            success: false,
                            err: errorHandler.throwSQLErr(err)
                        }

                        callback(response);

                    } else {
                        connection.query('INSERT INTO `user` SET ?;', {
                            id: data.id,
                            email: data.email,
                            password: data.password,
                            discriminator: 'client'
                        }, (err, results, fields) => {
                            if (err) {
                                response = {
                                    action: 'Create client',
                                    status: 'failed',
                                    code: 400,
                                    success: false,
                                    err: errorHandler.throwSQLErr(err)
                                }
                                callback(response);

                            }
                            if (results) {
                                data.link = `${urls.clientUrl}/${data.id}`
                                response = {
                                    action: 'Create client',
                                    status: 'ok',
                                    code: 200,
                                    success: true,
                                    message: 'client created',
                                    results: {
                                        client: data
                                    }
                                }
                            }
                            callback(response);
                        });
                    }

                });
            } else {
                response = {
                    action: 'create client',
                    status: 'failed',
                    code: 400,
                    success: false,
                    err: {
                        message: dataValidator.message,
                        resoulution: dataValidator.resolution,
                        err: dataValidator.err
                    }
                }

                callback(response);
            }
        }
    }
}