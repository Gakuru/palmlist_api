const connection = require('../db/connect');

const uuid = require('uuid/v1');

const urls = require('../utils/urls');

const pager = require('../utils/pager');
const transformer = require('../utils/transformer');

const validate = require('../utils/dataValidator');

const errorHandler = require('../utils/error');

const returnPaged = (result, nextStart, totalResultLength) => {
    return {
        pager: pager.getPager(result.length, totalResultLength, pager.options.size, nextStart, urls.providerUrl),
        data: transformer.transformProvider(result)
    };
};

module.exports = {
    get: (args) => {
        if (args.id) {
            connection.query('CALL `get_provider` (?);', [args.id], (err, results, fields) => {
                if (err) 
                    throw err;
                
                args.callback(transformer.transformProvider(results[0]));

            })
        } else {

            args.page = args.page
                ? (args.page < 0)
                    ? 1
                    : args.page
                : 1;

            pager.options.offSet = ((args.page * pager.options.size) - pager.options.size);

            connection.query('CALL `get_providers` (?,?);', [
                pager.options.offSet,
                (pager.options.size + 1)
            ], (err, results, fields) => {
                if (err) 
                    throw err;
                
                let totalResultLength = results[0].length;
                let resultSet = ((totalResultLength > pager.options.size)
                    ? results[0].splice(0, pager.options.size)
                    : results[0]);

                args.callback(returnPaged(resultSet, args.page, totalResultLength))

            })
        }
    },
    getNearBy: (args) => {
        //Some checking
        let response = undefined;
        if (args.coordinates) {

            if (args.coordinates.hasOwnProperty('lat') && args.coordinates.hasOwnProperty('lng')) {
                // response = {msg:'coordinates',args:args.coordinates};
                const {lat, lng} = args.coordinates;
                connection.query('CALL `get_providers_nearby` (?,?);', [
                    lat, lng
                ], (err, results, fields) => {
                    if (err) 
                        throw err;
                    
                    response = {
                        action: 'List providers nearby',
                        status: 'ok',
                        code: 200,
                        success: true,
                        message: 'Listing providers',
                        results: {
                            providers: results[0]
                        }
                    }

                    args.callback(response);

                });
            } else {
                response = {
                    action: 'List providers nearby',
                    status: 'failed',
                    code: 400,
                    success: false,
                    err: {
                        message: 'No properties supplied',
                        resoulution: 'Provide lat and lng to the query string'
                    }
                }

                args.callback(response);

            }

        } else {
            response = {
                action: 'List providers nearby',
                status: 'failed',
                code: 400,
                success: false,
                err: {
                    message: 'Could not list providers nearby',
                    resoulution: 'Provide a set of coordinates'
                }
            }

            args.callback(response);

        }

    },
    getProviderServices: (args) => {
        //Some checking
        let response = undefined;
        
        const { id } = args;
        
        connection.query('CALL `get_provider_services` (?);', [
            id
        ], (err, results, fields) => {
            if (err) 
                throw err;
            
            response = {
                action: 'List provider services',
                status: 'ok',
                code: 200,
                success: true,
                message: 'Listing provider services',
                results: {
                    services: results[0]
                }
            }

            args.callback(response);

        });

    },
    getProviderRequests: (args) => {
        //Some checking
        let response = undefined;
        
        const { id } = args;
        
        connection.query('CALL `get_provider_requests` (?);', [
            id
        ], (err, results, fields) => {
            if (err) 
                throw err;
            
            response = {
                action: 'List provider requests',
                status: 'ok',
                code: 200,
                success: true,
                message: 'Listing provider requests',
                results: {
                    requests: results[0]
                }
            }

            args.callback(response);

        });

    },
    save: (args) => {

        const {data, callback} = args;

        let response = undefined;

        if (data.id) {
            callback('update');
        } else {

            let dataValidator = validate.validatePost(data, 'provider');

            if (dataValidator.isValid) {

                data.id = uuid();

                connection.getConnection((err, connection) => {

                    connection.beginTransaction((err) => {

                        // Create the provider then Create the user account

                        connection.query('INSERT INTO `provider` SET ?;', {
                            id: data.id,
                            firstName: data.firstName,
                            lastName: data.lastName,
                            email: data.email,
                            phone: data.phone,
                            geo_lat: data.geo_lat,
                            geo_long: data.geo_long,
                            geo_accuracy: data.geo_accuracy
                        }, (err, results, fields) => {

                            if (err) {
                                response = {
                                    action: 'Create provider',
                                    status: 'failed',
                                    code: 400,
                                    success: false,
                                    err: errorHandler.throwSQLErr(err)
                                }
                                callback(response);
                            } else {
                                connection.query('INSERT INTO `user` SET ?', {
                                    id: data.id,
                                    email: data.email,
                                    password: data.password,
                                    discriminator: 'provider'
                                }, (err, results, fields) => {
                                    if (err) {
                                        response = {
                                            action: 'Create provider',
                                            status: 'failed',
                                            code: 400,
                                            success: false,
                                            err: errorHandler.throwSQLErr(err)
                                        }
                                    }
                                    if (results) {

                                        connection.query('INSERT INTO `user` SET ?', {
                                            id: data.id,
                                            username: data.email,
                                            password: data.password,
                                            discriminator: 'client'
                                        }, (err, results, fields) => {
                                            if (err) {
                                                response = {
                                                    action: 'Create provider',
                                                    status: 'failed',
                                                    code: 400,
                                                    success: false,
                                                    err: errorHandler.throwSQLErr(err)
                                                }
                                            }
                                            if (results) {
                                                data.link = `${urls.providerUrl}/${data.id}`
                                                response = {
                                                    action: 'Create provider',
                                                    status: 'ok',
                                                    code: 200,
                                                    success: true,
                                                    message: 'provider created',
                                                    results: {
                                                        provider: data
                                                    }
                                                }
                                            }

                                        });

                                    }

                                });

                            }

                        });

                        // Commit
                        connection.commit((err) => {
                            if (err) {
                                return connection.rollback(() => {
                                    response = {
                                        action: 'Create provider',
                                        status: 'failed',
                                        code: 400,
                                        success: false,
                                        err: errorHandler.throwSQLErr(err)
                                    }
                                });
                            } else {
                                callback(response);
                            }
                        });

                    });

                    connection.release();

                });

            } else {
                response = {
                    action: 'create provider',
                    status: 'failed',
                    code: 400,
                    success: false,
                    err: {
                        message: dataValidator.message,
                        resoulution: dataValidator.resolution,
                        err: dataValidator.err
                    }
                }

                callback(response);
            }
        }
    },
    addService: (args) => {
        const {data, callback} = args;

        let response = undefined;

        let dataValidator = validate.validatePost(data, 'provider_services');

        if (dataValidator.isValid) {

            data.id = uuid();

            connection.query('INSERT INTO `provider_services` SET ?;', data, (err, results, fields) => {
                if (err) {
                    response = {
                        action: 'Create provider',
                        status: 'failed',
                        code: 400,
                        success: false,
                        err: errorHandler.throwSQLErr(err)
                    }
                }
                if (results) {
                    response = {
                        action: 'Create provider service',
                        status: 'ok',
                        code: 200,
                        success: true,
                        message: 'service added to provider'
                    }
                }

                callback(response);

            })

        } else {
            response = {
                action: 'create provider service',
                status: 'failed',
                code: 400,
                success: false,
                err: {
                    message: dataValidator.message,
                    resoulution: dataValidator.resolution,
                    err: dataValidator.err
                }
            }

            callback(response);
        }

    },
    removeService: (args) => {
        const {data, callback} = args;

        let response = undefined;

        connection.query('DELETE FROM `provider_services` WHERE `id` = ? AND provider_id = ?;', [data.id, data.provider_id], (err, results, fields) => {
            if (err) {
                response = {
                    action: 'Remove provider service',
                    status: 'failed',
                    code: 400,
                    success: false,
                    err: errorHandler.throwSQLErr(err)
                }
            }
            if (results) {
                response = {
                    action: 'Remove provider service',
                    status: 'ok',
                    code: 200,
                    success: true,
                    message: 'Provider service removed'
                }
            }

            callback(response);

        });        

    }
}