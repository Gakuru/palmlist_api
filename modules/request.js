const connection = require('../db/connect');

const transformer = require('../utils/transformer');

const uuid = require('uuid/v1');

const requestStatus = {
    ACCEPTED: 'ACCEPTED',
    DECLINED: 'DECLINED', //Request is declined by a provider
    CANCELED: 'CANCELED', //Request is canceled by a client
    PENDING: 'PENDING',
    COMPLETED: 'COMPLETED'
}

module.exports = {
    getServiceProvidersNearby: (args) => {
        const {data, callback} = args;
        let response = undefined;

        connection.query("CALL `get_service_providers_nearby` (?,?,?);", [
            data.lat, data.lng, data.sid
        ], (err, results, fields) => {

            if (err) 
                throw err

            response = {
                action: 'List service providers near me',
                status: 'ok',
                code: 200,
                success: true,
                message: 'Listing available service providers near me',
                results: {
                    providers: transformer.transformProvidersNearBy(results[0])
                }
            }

            args.callback(response);

        });
    },
    request: (args) => {
        const { data, callback } = args
        
        data.id = uuid();
        data.status = requestStatus.PENDING;

        let response = undefined;

        connection.query('CALL `insert_request` (?,?,?,?,?,?,?)', [data.id,data.service_id,data.client_id,data.client_lat,data.client_lng,data.provider_id,data.status], (err, results, fields) => {
            if (err) throw err;

            connection.query('UPDATE `provider` SET `status` = ? WHERE `id` = ?;', [102, data.provider_id], (err, results, fields) => {
                if (err) throw err;

                response = results;

                callback(args);

            });

        });        

    }
}