function get(args) {
    const {data, callback} = args;

    if (data.id) {
        return {id: '12345678', name: 'abcdefg'}
    } else {
        return [
            {
                id: '12345678',
                name: 'abcdefg'
            }, {
                id: '87654321',
                name: 'gfedcba'
            }
        ]
    }

}

function testGetOne() {
    let _result = undefined;

    const result = get({
        data: {
            id: '12345678'
        },
        callback: (result) => {
            _result = result
        }
    });

    console.info(result);
}

function testGetMany() {}